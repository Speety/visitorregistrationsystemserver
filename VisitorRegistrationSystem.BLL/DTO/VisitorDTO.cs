﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO.BaseDTO;
using VisitorRegistrationSystem.DAL.Enteties;

namespace VisitorRegistrationSystem.BLL.DTO
{
    public class VisitorDTO : PersonDTO
    {
        public string ArrivingTime { get; set; }

        public string DepartureTime { get; set; }

        public string BadgeNumber { get; set; }

        public string CompanyName { get; set; }

        public virtual List<Appointment> Appointments { get; set; }
    }
}
