﻿using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegistrationSystem.BLL.DTO.BaseDTO
{
    public class PersonDTO : EntityBase
    {
        public string Name { get; set; }

        public string LastName { get; set; }
    }
}
