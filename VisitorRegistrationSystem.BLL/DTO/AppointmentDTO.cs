﻿using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegistrationSystem.BLL.DTO
{
    public class AppointmentDTO : EntityBase
    {
        public string Number { get; set; }

        public VisitorDTO VisitorDTO { get; set; }

        public EmployeeDTO EmployeeDTO { get; set; }

        public int VisitorId { get; set; }

        public int EmployeeId { get; set; }

        public string TimeOfVisit { get; set; }
    }
}
