﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;

namespace VisitorRegistrationSystem.BLL.Interfaces
{
    public interface IVisitorService
    {
        void AddVisitor(VisitorDTO visitorDto);
        void DeleteVisitor(int? id);
        VisitorDTO GetVisitor(int? id);
        IEnumerable<VisitorDTO> GetVisitors();
        void EditVisitor(VisitorDTO visitorDto);
        int AddVisitorGetId(VisitorDTO visitorDTO);
        void Dispose();
    }
}
