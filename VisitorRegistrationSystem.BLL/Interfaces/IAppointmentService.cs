﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;

namespace VisitorRegistrationSystem.BLL.Interfaces
{
    public interface IAppointmentService
    {
        void AddAppointment(AppointmentDTO appointmentDto);
        void DeleteAppointment(int? id);
        AppointmentDTO GetAppointment(int? id);
        IEnumerable<AppointmentDTO> GetAppointments();
        void EditAppointment(AppointmentDTO appointmentDto);
        AppointmentDTO GetAppointmentByNumber(string appointmentNumber);
        void Dispose();
    }
}
