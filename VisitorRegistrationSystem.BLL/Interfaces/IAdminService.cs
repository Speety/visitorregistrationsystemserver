﻿using System.Collections.Generic;
using VisitorRegistrationSystem.BLL.DTO;

namespace VisitorRegistrationSystem.BLL.Interfaces
{
    public interface IAdminService
    {
        void AddAdmin(AdminDTO adminDto);
        void DeleteAdmin(int? id);
        AdminDTO GetAdmin(int? id);
        IEnumerable<AdminDTO> GetAdmins();
        void EditAdmin(AdminDTO adminDto);
        AdminDTO GetByLogin(string login);
        void Dispose();
    }
}
