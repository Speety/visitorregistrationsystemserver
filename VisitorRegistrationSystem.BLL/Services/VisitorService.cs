﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Services
{
    public class VisitorService : IVisitorService
    {
        IVisitorRepository visitorRepository { get; set; }

        public VisitorService(IVisitorRepository rep)
        {
            //// Настройка AutoMapper
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<ApplicationDTO, Application>();
            //    cfg.CreateMap<Application, ApplicationDTO>();
            //});

            visitorRepository = rep;
        }

        public void AddVisitor(VisitorDTO visitorDto)
        {
            // validate
            if (visitorDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // mapping
                var visitor = Mapper.Map<VisitorDTO, Visitor>(visitorDto);
                this.visitorRepository.Visitors.Add(visitor);
                this.visitorRepository.Visitors.Save();
            }
        }

        public IEnumerable<VisitorDTO> GetVisitors()
        {
            // mapping
            return Mapper.Map<IEnumerable<Visitor>, List<VisitorDTO>>(this.visitorRepository.Visitors.GetAll());
        }

        public VisitorDTO GetVisitor(int? id)
        {
            if (id != null)
            {
                var visitor = this.visitorRepository.Visitors.GetById(id.Value);
                if (visitor == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Visitor, VisitorDTO>(visitor);
            }

            throw new MapperValidationException("id not found", "");
        }

        public int AddVisitorGetId(VisitorDTO visitorDTO)
        {
            if (visitorDTO != null)
            {
                this.visitorRepository.Visitors.Add(Mapper.Map<VisitorDTO, Visitor>(visitorDTO));
                this.visitorRepository.Visitors.Save();

                var appintmentBack = this.visitorRepository.Visitors.GetAll().FirstOrDefault(a => a.BadgeNumber == visitorDTO.BadgeNumber);
                return appintmentBack.Id;
            }

            return 0;
        }

        public void DeleteVisitor(int? id)
        {
            // validation
            if (id == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                var visitor = visitorRepository.Visitors.GetById(id.Value);
                this.visitorRepository.Visitors.Delete(visitor);
                this.visitorRepository.Visitors.Save();
            }
        }

        public void EditVisitor(VisitorDTO visitorDto)
        {
            // validation
            if (visitorDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // process mapping
                var visitorToEdit = Mapper.Map<VisitorDTO, Visitor>(visitorDto);
                this.visitorRepository.Visitors.Edit(visitorToEdit);
                this.visitorRepository.Visitors.Save();
            }
        }

        public void Dispose()
        {
            this.visitorRepository.Dispose();
        }
    }
}
