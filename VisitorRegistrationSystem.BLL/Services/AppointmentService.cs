﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Services
{
    public class AppointmentService : IAppointmentService
    {
        IAppointmentRepository appointmentRepository { get; set; }

        public AppointmentService(IAppointmentRepository rep)
        {
            //// Настройка AutoMapper
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<ApplicationDTO, Application>();
            //    cfg.CreateMap<Application, ApplicationDTO>();
            //});

            appointmentRepository = rep;
        }

        public void AddAppointment(AppointmentDTO appointmentDto)
        {
            // validate
            if (appointmentDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // mapping
                this.appointmentRepository.Appointments.Add(Mapper.Map<AppointmentDTO, Appointment>(appointmentDto));
                this.appointmentRepository.Appointments.Save();
            }
        }

        public AppointmentDTO GetAppointmentByNumber(string appointmentNumber)
        {
            if (!string.IsNullOrEmpty(appointmentNumber))
            {
                var appointment = this.appointmentRepository.Appointments.GetAll().FirstOrDefault(a => a.Number == appointmentNumber);
                if (appointment == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Appointment, AppointmentDTO>(appointment);
            }

            throw new MapperValidationException("appointmentNumber not found", "");
        }
        

        public IEnumerable<AppointmentDTO> GetAppointments()
        {
            // mapping
            return Mapper.Map<IEnumerable<Appointment>, List<AppointmentDTO>>(this.appointmentRepository.Appointments.GetAll());
        }

        public AppointmentDTO GetAppointment(int? id)
        {
            if (id != null)
            {
                var appointment = this.appointmentRepository.Appointments.GetById(id.Value);
                if (appointment == null)
                    throw new MapperValidationException("application not found", "");

                // mapping
                return Mapper.Map<Appointment, AppointmentDTO>(appointment);
            }

            throw new MapperValidationException("id not found", "");
        }

        public void DeleteAppointment(int? id)
        {
            // validation
            if (id == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                var appointment = appointmentRepository.Appointments.GetById(id.Value);
                this.appointmentRepository.Appointments.Delete(appointment);
                this.appointmentRepository.Appointments.Save();
            }
        }

        public void EditAppointment(AppointmentDTO appointmentDto)
        {
            // validation
            if (appointmentDto == null)
            {
                throw new MapperValidationException("application can't be added because it's null", "");
            }
            else
            {
                // process mapping
                var appointmentToEdit = Mapper.Map<AppointmentDTO, Appointment>(appointmentDto);
                this.appointmentRepository.Appointments.Edit(appointmentToEdit);
                this.appointmentRepository.Appointments.Save();
            }
        }

        public void Dispose()
        {
            this.appointmentRepository.Dispose();
        }
    }
}
