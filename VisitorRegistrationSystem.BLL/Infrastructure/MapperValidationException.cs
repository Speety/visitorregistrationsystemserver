﻿using System;

namespace VisitorRegistrationSystem.BLL.Infrastructure
{
    public class MapperValidationException : Exception
    {
        public string Property { get; protected set; }
        public MapperValidationException(string message, string prop) : base(message)
        {
            Property = prop;
        }
    }
}
