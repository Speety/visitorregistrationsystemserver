﻿using Ninject.Modules;
using VisitorRegistrationSystem.DAL.Data.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;

namespace VisitorRegistrationSystem.BLL.Infrastructure
{
    public class IOCServiceModule : NinjectModule
    {
        private string connectionString;

        public IOCServiceModule(string connection)
        {
            connectionString = connection;
        }

        public override void Load()
        {
            Bind<IAdminRepository>().To<AdminRepository>().WithConstructorArgument(connectionString);
            Bind<IAppointmentRepository>().To<AppointmentRepository>().WithConstructorArgument(connectionString);
            Bind<IEmployeeRepository>().To<EmployeeRepository>().WithConstructorArgument(connectionString);
            Bind<IVisitorRepository>().To<VisitorRepository>().WithConstructorArgument(connectionString);
        }
    }
}
