﻿namespace VisitorRegistrationSystem.DAL.Enums
{
    public enum AccessLevelEnum
    {
        Admin = 1,
        Moderator = 2
    }
}
