﻿using System;
using VisitorRegistrationSystem.DAL.Data.Repositories.Base;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Data.Repositories
{
    public class VisitorRepository : IVisitorRepository
    {
        private DataContext dbContext;
        private IRepository<Visitor> visitorRepository;

        public VisitorRepository(string connectionString)
        {
            dbContext = new DataContext(connectionString);
        }

        public IRepository<Visitor> Visitors
        {
            get
            {
                if (visitorRepository == null)
                    visitorRepository = new Repository<Visitor>(dbContext);
                return visitorRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
