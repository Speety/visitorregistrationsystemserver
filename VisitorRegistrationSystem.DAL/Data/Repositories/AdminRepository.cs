﻿using System;
using VisitorRegistrationSystem.DAL.Data.Repositories.Base;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Data.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        private DataContext dbContext;
        private IRepository<Admin> adminRepository;

        public AdminRepository(string connectionString)
        {
            dbContext = new DataContext(connectionString);
        }

        public IRepository<Admin> Admins
        {
            get
            {
                if (adminRepository == null)
                    adminRepository = new Repository<Admin>(dbContext);
                return adminRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
