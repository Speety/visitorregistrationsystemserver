﻿using System;
using VisitorRegistrationSystem.DAL.Data.Repositories.Base;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Data.Repositories
{
    public class AppointmentRepository : IAppointmentRepository
    {
        private DataContext dbContext;
        private IRepository<Appointment> appointmentRepository;

        public AppointmentRepository(string connectionString)
        {
            dbContext = new DataContext(connectionString);
        }

        public IRepository<Appointment> Appointments
        {
            get
            {
                if (appointmentRepository == null)
                    appointmentRepository = new Repository<Appointment>(dbContext);
                return appointmentRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
