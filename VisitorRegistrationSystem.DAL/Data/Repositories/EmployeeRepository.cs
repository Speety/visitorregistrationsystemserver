﻿using System;
using VisitorRegistrationSystem.DAL.Data.Repositories.Base;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories;
using VisitorRegistrationSystem.DAL.Interfaces.Repositories.Base;

namespace VisitorRegistrationSystem.DAL.Data.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private DataContext dbContext;
        private IRepository<Employee> employeeRepository;

        public EmployeeRepository(string connectionString)
        {
            dbContext = new DataContext(connectionString);
        }

        public IRepository<Employee> Employees
        {
            get
            {
                if (employeeRepository == null)
                    employeeRepository = new Repository<Employee>(dbContext);
                return employeeRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
