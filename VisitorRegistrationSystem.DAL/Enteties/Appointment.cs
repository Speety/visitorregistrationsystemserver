﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Appointment : EntityBase
    {
        [Required]
        public string Number { get; set; }

        [Required]
        public string TimeOfVisit { get; set; }

        public int VisitorId { get; set; }

        public int EmployeeId { get; set; }

        public virtual Visitor Visitor { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
