﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Employee : Person
    {
        public Employee()
        {
            Appointments = new Collection<Appointment>();
        }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Position { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public string PhotoPath { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
