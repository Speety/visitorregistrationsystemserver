﻿using System.ComponentModel.DataAnnotations;

namespace VisitorRegistrationSystem.DAL.Enteties.Base.Kernel
{
    public abstract class EntityBase
    {
        [Key]
        [Required]
        public int Id { get; set; }
    }
}
