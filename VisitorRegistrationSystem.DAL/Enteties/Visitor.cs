﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base;

namespace VisitorRegistrationSystem.DAL.Enteties
{
    public class Visitor : Person
    {
        public Visitor()
        {
            Appointments = new Collection<Appointment>();
        }

        [Required]
        public string ArrivingTime { get; set; }

        public string DepartureTime { get; set; }

        [Required]
        public string BadgeNumber { get; set; }

        public string CompanyName { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
