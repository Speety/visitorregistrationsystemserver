﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class AppointmentViewModelPanel : EntityBase
    {
        [Display(Name = "Appointment number")]
        public string Number { get; set; }

        [Display(Name = "Visitor name")]
        public string VisitorName { get; set; }

        [Display(Name = "Visitor badge number")]
        public string VisitorBadge { get; set; }

        [Display(Name = "Employee name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Check-in of visit")]
        public string TimeOfVisit { get; set; }

        [Display(Name = "Check-out time")]
        public string DepartureTime { get; set; }
    }
}