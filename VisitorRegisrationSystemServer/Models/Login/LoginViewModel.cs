﻿using System.ComponentModel.DataAnnotations;

namespace VisitorRegisrationSystemServer.Models.Login
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter Login")]
        [StringLength(20, ErrorMessage = "Login length should be not more then 20 characters")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters or numbers are only anllowed")]
        //[MinLength(6, ErrorMessage = "Login minimum length is 6 characters")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(20, ErrorMessage = "Login length should be not more then 20 characters")]
        //[MinLength(6, ErrorMessage = "Password minimum length is 6 characters")]
        public string Password { get; set; }
    }
}