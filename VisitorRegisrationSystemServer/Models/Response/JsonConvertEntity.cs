﻿using System;

namespace VisitorRegisrationSystemServer.Models.Response
{
    public class JsonConvertEntity
    {
        public EmployeeToSave Employee { get; set; }

        public VisitorToSave Visitor { get; set; }
    }

    public class EmployeeToSave
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
    }

    public class VisitorToSave
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string CompanyName { get; set; }
        public DateTime CheckInDate { get; set; }
    }
}