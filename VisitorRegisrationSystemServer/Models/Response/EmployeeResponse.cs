﻿using System.ComponentModel.DataAnnotations;

namespace VisitorRegisrationSystemServer.Models.Response
{
    public class EmployeeResponse
    {
        [Key]
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public byte[] Photo { get; set; }
    }
}