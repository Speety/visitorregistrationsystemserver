﻿namespace VisitorRegisrationSystemServer.Models.Response
{
    public class VisitorResponse
    {
        public int Id { get; set; }
        public string BadgeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string CheckInDate { get; set; }
    }
}