﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class VisitorViewModel : EntityBase
    {
        [Required(ErrorMessage = "Please provide the name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide the last name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        public string ArrivingTime { get; set; }

        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        public string DepartureTime { get; set; }

        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters and numbers are only anllowed")]
        [StringLength(5, ErrorMessage = "Number length should be not more then 5 characters")]
        [MinLength(5, ErrorMessage = "Number minimum length is 5 characters")]
        public string BadgeNumber { get; set; }

        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters and numbers are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string CompanyName { get; set; }
    }
}