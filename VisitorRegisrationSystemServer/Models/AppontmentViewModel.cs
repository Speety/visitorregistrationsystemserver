﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class AppontmentViewModel : EntityBase
    {
        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^[a-zA-Z0-9_.-]*$", ErrorMessage = "Letters and numbers are only anllowed")]
        [StringLength(5, ErrorMessage = "Number length should be not more then 5 characters")]
        [MinLength(5, ErrorMessage = "Number minimum length is 5 characters")]
        [Display(Name = "Appointment number")]
        public string Number { get; set; }

        [Required(ErrorMessage = "Please provide the time of visit")]
        [RegularExpression(@"^(0[1-9]|1[0-2])/(0[1-9]|1[0-9]|2[0-9]|3[0-1])/[2][0][1-3][0-9] ([0-9]|0[0-9]|1[0-2]):[0-5][0-9] ([AaPp][Mm])$", ErrorMessage = "Wrong formatting")]
        [Display(Name = "Time of visit")]
        public string TimeOfVisit { get; set; }

        [Required(ErrorMessage = "Please provide the name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "Visitor name")]
        public string VisitorName { get; set; }

        [Required(ErrorMessage = "Please provide the last name of visitor")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "Visitor last name")]
        public string VisitorLastName { get; set; }

        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        [Display(Name = "Visitor comtany")]
        public string CompanyName { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Register an Employee first")]
        public int EmployeeId{ get; set; }

        public string EmployeeName { get; set; }

        [Required]
        public int VisitorId { get; set; }
    }
}