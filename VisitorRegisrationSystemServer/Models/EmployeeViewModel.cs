﻿using System.ComponentModel.DataAnnotations;
using VisitorRegistrationSystem.DAL.Enteties.Base.Kernel;

namespace VisitorRegisrationSystemServer.Models
{
    public class EmployeeViewModel : EntityBase
    {
        [Required(ErrorMessage = "Please provide the name of employee")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please provide the last name of employee")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = "Not an email format")]
        [Required(ErrorMessage = "Please provide the last name of employee")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Please provide the position of employee in a company")]
        [StringLength(50, ErrorMessage = "Name length should be not more then 50 characters")]
        [RegularExpression(@"^[a-zA-Z_.-]*$", ErrorMessage = "Letters are only anllowed")]
        [MinLength(2, ErrorMessage = "Name minimum length is 2 characters")]
        public string Position { get; set; }

        [Required(ErrorMessage = "Please provide phone number of employee")]
        [Phone(ErrorMessage = "Phone number format should be +")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string PhotoPath { get; set; }
    }
}