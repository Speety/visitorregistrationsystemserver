﻿using AutoMapper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using Ninject.Web.WebApi;
using Ninject.Web.WebApi.Filter;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using VisitorRegisrationSystemServer.MessageHandlers;
using VisitorRegisrationSystemServer.Models;
using VisitorRegisrationSystemServer.Models.Response;
using VisitorRegisrationSystemServer.Security;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.DTO.BaseDTO;
using VisitorRegistrationSystem.BLL.Infrastructure;
using VisitorRegistrationSystem.DAL.Enteties;
using VisitorRegistrationSystem.DAL.Enteties.Base;

namespace VisitorRegisrationSystemServer
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configuration.MessageHandlers.Add(new APIKeyHandler());
            GlobalConfiguration.Configuration.MessageHandlers.Add(new AuthHandler());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // dependency injection
            NinjectModule orderModule = new IOCApplicationModule();
            NinjectModule serviceModule = new IOCServiceModule("Connection");
            var kernel = new StandardKernel(orderModule, serviceModule);
            kernel.Unbind<ModelValidatorProvider>();

            DependencyResolver.SetResolver(new Ninject.Web.Mvc.NinjectDependencyResolver(kernel));
            kernel.Bind<DefaultFilterProviders>().ToSelf().WithConstructorArgument(GlobalConfiguration.Configuration.Services.GetFilterProviders());
            GlobalConfiguration.Configuration.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(kernel);

            // AutoMapper Config
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Person, PersonDTO>().Include<Employee, EmployeeDTO>().Include<Visitor, VisitorDTO>();
                cfg.CreateMap<AdminDTO, Admin>();
                cfg.CreateMap<Admin, AdminDTO>();
                cfg.CreateMap<EmployeeDTO, Employee>();
                cfg.CreateMap<Employee, EmployeeDTO>();
                cfg.CreateMap<VisitorDTO, Visitor>();
                cfg.CreateMap<Visitor, VisitorDTO>();

                //map specific info for employee
                cfg.CreateMap<AppointmentDTO, Appointment>().ForMember(appointment => appointment.Employee,
                    opt => opt.MapFrom(a => a.EmployeeDTO)).ForMember(appointment => appointment.Visitor,
                    opt => opt.MapFrom(a => a.VisitorDTO));

                //map specific info for employee
                cfg.CreateMap<Appointment, AppointmentDTO>().ForMember(appointmentDTO => appointmentDTO.EmployeeDTO,
                    opt => opt.MapFrom(a => a.Employee)).ForMember(appointmentDTO => appointmentDTO.VisitorDTO,
                    opt => opt.MapFrom(a => a.Visitor));

                //map specific info for employee
                cfg.CreateMap<AppointmentDTO, AppointmentViewModelPanel>().ForMember(appointmentViewModel => appointmentViewModel.EmployeeName,
                    map => map.MapFrom(a => a.EmployeeDTO.Name + " " + a.EmployeeDTO.LastName)).ForMember(appointmentViewModel => appointmentViewModel.VisitorName,
                    map => map.MapFrom(a => a.VisitorDTO.Name + " " + a.VisitorDTO.LastName)).ForMember(appointmentViewModel => appointmentViewModel.VisitorBadge,
                    map => map.MapFrom(a => a.VisitorDTO.BadgeNumber)).ForMember(appointmentViewModel => appointmentViewModel.DepartureTime,
                    map => map.MapFrom(a => a.VisitorDTO.DepartureTime));

                cfg.CreateMap<AdminViewModel, AdminDTO>();
                cfg.CreateMap<AdminDTO, AdminViewModel>();

                cfg.CreateMap<AppontmentViewModel, AppointmentDTO>();
                cfg.CreateMap<AppointmentDTO, AppontmentViewModel>().ForMember(appointmentView => appointmentView.CompanyName,
                    opt => opt.MapFrom(a => a.VisitorDTO.CompanyName)).ForMember(appointmentView => appointmentView.VisitorName,
                    opt => opt.MapFrom(a => a.VisitorDTO.Name)).ForMember(appointmentView => appointmentView.VisitorLastName,
                    opt => opt.MapFrom(a => a.VisitorDTO.LastName)).ForMember(appointmentView => appointmentView.VisitorId,
                    opt => opt.MapFrom(a => a.VisitorDTO.Id)).ForMember(appointmentView => appointmentView.EmployeeId,
                    opt => opt.MapFrom(a => a.EmployeeDTO.Id)).ForMember(appointmentView => appointmentView.EmployeeName,
                    opt => opt.MapFrom(a => a.EmployeeDTO.Name + " " + a.EmployeeDTO.LastName));

                cfg.CreateMap<EmployeeViewModel, EmployeeDTO>();
                cfg.CreateMap<EmployeeDTO, EmployeeViewModel>();

                cfg.CreateMap<VisitorViewModel, VisitorDTO>();
                cfg.CreateMap<VisitorDTO, VisitorViewModel>();
            });
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null || authCookie.Value == "")
                return;

            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }

            // retrieve roles from UserData
            string[] roles = authTicket.UserData.Split(';');

            if (Context.User != null)
                Context.User = new GenericPrincipal(Context.User.Identity, roles);
        }
    }
}
