﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VisitorRegisrationSystemServer.Helpers;
using VisitorRegisrationSystemServer.Helpers.Providers;
using VisitorRegisrationSystemServer.Models;
using VisitorRegisrationSystemServer.Models.Login;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Interfaces;
using VisitorRegistrationSystem.DAL.Enums;

namespace VisitorRegisrationSystemServer.Controllers
{
    public class AdminController : Controller
    {
        IAdminService adminServ;
        IAppointmentService appointmentServ;
        IEmployeeService employeeServ;
        IVisitorService visitorServ;
        public AdminController(IAdminService adminServ, IAppointmentService appointmentServ, IEmployeeService employeeServ, IVisitorService visitorServ)
        {
            this.adminServ = adminServ;
            this.appointmentServ = appointmentServ;
            this.employeeServ = employeeServ;
            this.visitorServ = visitorServ;
        }

        public ActionResult Login()
        {
            LoginViewModel logModel = new LoginViewModel();
            return View(logModel);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isLoggedIn = false;
                bool isDefaultAdminInput = (model.Login == "admin" && model.Password == "123");
                AdminDTO admin = this.adminServ.GetByLogin(model.Login);

                if (isDefaultAdminInput || admin != null)
                {
                    //Check if default admin created(first request to application)
                    if (isDefaultAdminInput && admin == null)
                    {
                        //default admin isn't created
                        admin = new AdminDTO()
                        {
                            Login = model.Login,
                            Password = System.Web.Helpers.Crypto.HashPassword(model.Password),
                            AccessLevel = AccessLevelEnum.Admin
                        };

                        this.adminServ.AddAdmin(admin);

                        //admin is created
                        isLoggedIn = true;
                    }
                    else
                    {
                        //default admin already exists
                        //user input isn't equal to default admin credentials

                        if (System.Web.Helpers.Crypto.VerifyHashedPassword(admin.Password, model.Password))
                        {
                            //password is verified
                            isLoggedIn = true;
                        }
                        else
                        {
                            ModelState.AddModelError("Password", "Password is not correct");
                        }
                    }

                    if (isLoggedIn)
                    {
                        FormsAuthentication.SetAuthCookie(model.Login, true);

                        var authTicket = new FormsAuthenticationTicket(
                                         1,                             // version
                                         admin.Login,                      // user name
                                         DateTime.Now,                  // created
                                         DateTime.Now.AddMinutes(20),   // expires
                                         false,                          // persistent?
                                         admin.AccessLevel.ToString()    // can be used to store roles
                                         );

                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        HttpContext.Response.Cookies.Add(authCookie);

                        //get username from ticket
                        return RedirectToAction("MainPage");
                    }
                }
                else
                {
                    ModelState.AddModelError("Login", "Current user is not registered in the system");
                }
            }

            return View("Login");
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        #region Appointment

        //Appoinment
        [Authorize]
        public ActionResult MainPage()
        {
            try
            {
                IEnumerable<AppointmentDTO> appointmentDtos = this.appointmentServ.GetAppointments();
                var appointments = Mapper.Map<IEnumerable<AppointmentDTO>, List<AppointmentViewModelPanel>>(appointmentDtos);
                return View(appointments);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult GenerateReport(ReportEntity report)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IEnumerable<AppointmentDTO> appointmentDtos = this.appointmentServ.GetAppointments();
                    var appointments = Mapper.Map<IEnumerable<AppointmentDTO>, List<AppointmentViewModelPanel>>(appointmentDtos);

                    ViewBag.ReportEntity = new ReportEntity();

                    return View(appointments);
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return RedirectToAction("MainPage");
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateAppoinment()
        {
            try
            {
                AppontmentViewModel newAppointment = new AppontmentViewModel()
                {
                    Number = BadgeGenerator.GenerateNubmer(),
                    TimeOfVisit = DateTime.Now.ToString("MM/dd/yyyy h:mm tt")
                };

                ViewBag.EmployeesDTO = new SelectList(this.employeeServ.GetEmployees(), "Id", "Name");

                return View(newAppointment);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateAppoinment(AppontmentViewModel appointment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tempVisitor = new VisitorDTO()
                    {
                        Name = appointment.VisitorName,
                        LastName = appointment.VisitorLastName,
                        ArrivingTime = appointment.TimeOfVisit,
                        CompanyName = appointment.CompanyName,
                        BadgeNumber = appointment.Number,
                    };

                    var visitorId = this.visitorServ.AddVisitorGetId(tempVisitor);

                    var newAppointment = new AppointmentDTO()
                    {
                        EmployeeId = appointment.EmployeeId,
                        VisitorId = visitorId,
                        Number = tempVisitor.BadgeNumber,
                        TimeOfVisit = tempVisitor.ArrivingTime
                    };

                    this.appointmentServ.AddAppointment(newAppointment);

                    await InformationUpdateProvider.ChangeStatus("visitors");

                    return RedirectToAction("MainPage");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            ViewBag.EmployeesDTO = new SelectList(this.employeeServ.GetEmployees(), "Id", "Name");
            return View(appointment);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditAppointment(int id)
        {
            try
            {
                ViewBag.EmployeesDTO = new SelectList(this.employeeServ.GetEmployees(), "Id", "Name");
                var appointmentViewModel = Mapper.Map<AppointmentDTO, AppontmentViewModel>(this.appointmentServ.GetAppointment(id));
                return View(appointmentViewModel);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditAppointment(AppontmentViewModel appointment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var appointmentDto = Mapper.Map<AppontmentViewModel, AppointmentDTO>(appointment);
                    this.appointmentServ.EditAppointment(appointmentDto);

                    var visitorToEdit = this.visitorServ.GetVisitor(appointment.VisitorId);
                    visitorToEdit.Name = appointment.VisitorName;
                    visitorToEdit.LastName = appointment.VisitorLastName;
                    visitorToEdit.CompanyName = appointment.CompanyName;
                    this.visitorServ.EditVisitor(visitorToEdit);

                    await InformationUpdateProvider.ChangeStatus("visitors");

                    return RedirectToAction("MainPage");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            ViewBag.EmployeesDTO = new SelectList(this.employeeServ.GetEmployees(), "Id", "Name");
            return View(appointment);
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> DeleteAppointment(int id)
        {
            try
            {
                this.appointmentServ.DeleteAppointment(id);

                await InformationUpdateProvider.ChangeStatus("visitors");

                return RedirectToAction("MainPage");
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult DetailsAppointment(int id)
        {
            try
            {
                var appointmentViewModel = Mapper.Map<AppointmentDTO, AppontmentViewModel>(this.appointmentServ.GetAppointment(id));
                return View(appointmentViewModel);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        #endregion

        #region Admin

        //Admin
        [Authorize]
        public ActionResult AdminList()
        {
            try
            {
                IEnumerable<AdminDTO> adminDtos = this.adminServ.GetAdmins();
                return View(adminDtos);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult CreateAdmin()
        {
            return View(new AdminViewModel());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult CreateAdmin(AdminViewModel admin)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var passEnc = System.Web.Helpers.Crypto.HashPassword(admin.Password);
                    admin.Password = passEnc;
                    AdminDTO adminDto = Mapper.Map<AdminViewModel, AdminDTO>(admin);
                    this.adminServ.AddAdmin(adminDto);

                    return RedirectToAction("AdminList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(new AdminViewModel());
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult EditAdmin(int id)
        {
            try
            {
                var adminViewModel = Mapper.Map<AdminDTO, AdminViewModel>(this.adminServ.GetAdmin(id));
                return View(adminViewModel);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult EditAdmin(AdminViewModel editedAdmin)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AdminDTO admin = this.adminServ.GetAdmin(editedAdmin.Id);
                    if(admin.Password != editedAdmin.Password)
                    {
                        editedAdmin.Password = System.Web.Helpers.Crypto.HashPassword(editedAdmin.Password);
                    }

                    var adminViewModel = Mapper.Map<AdminViewModel, AdminDTO>(editedAdmin);
                    this.adminServ.EditAdmin(adminViewModel);

                    if(admin.AccessLevel != editedAdmin.AccessLevel)
                    {
                        HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

                        var authTicket = new FormsAuthenticationTicket(
                                         1,                             // version
                                         editedAdmin.Login,                      // user name
                                         DateTime.Now,                  // created
                                         DateTime.Now.AddMinutes(20),   // expires
                                         false,                          // persistent?
                                         editedAdmin.AccessLevel.ToString()    // can be used to store roles
                                         );

                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpContext.Response.Cookies.Set(authCookie);

                        return RedirectToAction("Logout");
                    }

                    return RedirectToAction("AdminList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(editedAdmin);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DeleteAdmin(int id)
        {
            try
            {
                this.adminServ.DeleteAdmin(id);
                return RedirectToAction("AdminList");
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        public ActionResult DetailsAdmin(int id)
        {
            try
            {
                var adminDto = Mapper.Map<AdminDTO, AdminViewModel>(this.adminServ.GetAdmin(id));
                return View(adminDto);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        #endregion

        #region Employee

        //Admin
        [Authorize]
        public ActionResult EmployeeList()
        {
            try
            {
                IEnumerable<EmployeeDTO> employeeDtos = this.employeeServ.GetEmployees();
                var appointments = Mapper.Map<IEnumerable<EmployeeDTO>, List<EmployeeViewModel>>(employeeDtos);
                return View(appointments);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateEmployee()
        {
            return View(new EmployeeViewModel());
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmployeeViewModel employee, HttpPostedFileBase photoImage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (photoImage != null)
                    {
                        employee.PhotoPath = FileProvider.SaveFile(photoImage);
                    }

                    var employeeDto = Mapper.Map<EmployeeViewModel, EmployeeDTO>(employee);
                    this.employeeServ.AddEmployee(employeeDto);

                    await InformationUpdateProvider.ChangeStatus("employees");

                    return RedirectToAction("EmployeeList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(new EmployeeViewModel());
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditEmployee(int id)
        {
            try
            {
                var employeeViewModel = Mapper.Map<EmployeeDTO, EmployeeViewModel>(this.employeeServ.GetEmployee(id));
                return View(employeeViewModel);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditEmployee(EmployeeViewModel employee, HttpPostedFileBase photoImage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //CHECK FOREIGHN KEY
                    var employeeDto = Mapper.Map<EmployeeViewModel, EmployeeDTO>(employee);
                    

                    if (photoImage != null)
                    {
                        if (!string.IsNullOrEmpty(employeeDto.PhotoPath))
                        {
                            FileProvider.DeleteFile(employeeDto.PhotoPath);
                        }
                        employeeDto.PhotoPath = FileProvider.SaveFile(photoImage);
                    }

                    this.employeeServ.EditEmployee(employeeDto);

                    await InformationUpdateProvider.ChangeStatus("employees");

                    return RedirectToAction("EmployeeList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(employee);
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> DeleteEmployee(int id)
        {
            try
            {
                this.employeeServ.DeleteEmployee(id);

                await InformationUpdateProvider.ChangeStatus("employees");

                return RedirectToAction("EmployeeList");
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult DetailsEployee(int id)
        {
            try
            {
                var employeeDto = Mapper.Map<EmployeeDTO, EmployeeViewModel>(this.employeeServ.GetEmployee(id));
                return View(employeeDto);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        #endregion

        #region Visitor

        //Admin
        [Authorize]
        public ActionResult VisitorList()
        {
            try
            {
                IEnumerable<VisitorDTO> visitorDtos = this.visitorServ.GetVisitors();
                var visitors = Mapper.Map<IEnumerable<VisitorDTO>, List<VisitorViewModel>>(visitorDtos);
                return View(visitors);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateVisitor()
        {
            var newVisitor = new VisitorViewModel()
            {
                BadgeNumber = BadgeGenerator.GenerateNubmer(),
                ArrivingTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt")
            };

            return View(newVisitor);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreateVisitor(VisitorViewModel visitor)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var visitorDto = Mapper.Map<VisitorViewModel, VisitorDTO>(visitor);
                    this.visitorServ.AddVisitor(visitorDto);
                    return RedirectToAction("VisitorList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(new VisitorViewModel());
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditVisitor(int id)
        {
            try
            {
                var vistiorViewModel = Mapper.Map<VisitorDTO, VisitorViewModel>(this.visitorServ.GetVisitor(id));
                return View(vistiorViewModel);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditVisitor(VisitorViewModel visitor)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var visitorDto = Mapper.Map<VisitorViewModel, VisitorDTO>(visitor);
                    this.visitorServ.EditVisitor(visitorDto);

                    await InformationUpdateProvider.ChangeStatus("visitors");
                    return RedirectToAction("VisitorList");
                }
                catch (Exception e)
                {
                    return View("Error", e.Message);
                }
            }

            return View(visitor);
        }

        [Authorize]
        [HttpGet]
        public ActionResult DeleteVisitor(int id)
        {
            try
            {
                this.visitorServ.DeleteVisitor(id);
                return RedirectToAction("VisitorList");
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult DetailsVisitor(int id)
        {
            try
            {
                var visitorDto = Mapper.Map<VisitorDTO, VisitorViewModel>(this.visitorServ.GetVisitor(id));
                return View(visitorDto);
            }
            catch (Exception e)
            {
                return View("Error", e.Message);
            }
        }

        #endregion

        //Dispose
        protected override void Dispose(bool disposing)
        {
            this.adminServ.Dispose();
            this.appointmentServ.Dispose();
            this.employeeServ.Dispose();
            this.visitorServ.Dispose();
            base.Dispose(disposing);
        }

        
    }
}