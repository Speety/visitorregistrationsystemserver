﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VisitorRegisrationSystemServer.Helpers;
using VisitorRegisrationSystemServer.Helpers.Providers;
using VisitorRegisrationSystemServer.Models.Response;
using VisitorRegistrationSystem.BLL.DTO;
using VisitorRegistrationSystem.BLL.Interfaces;

namespace VisitorRegisrationSystemServer.Controllers
{
    public class ValuesController : ApiController
    {
        IAppointmentService appointmentServ;
        IEmployeeService employeeServ;
        IVisitorService visitorServ;
        public ValuesController(IAppointmentService appointmentServ, IEmployeeService employeeServ, IVisitorService visitorServ)
        {
            this.appointmentServ = appointmentServ;
            this.employeeServ = employeeServ;
            this.visitorServ = visitorServ;
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [ActionName("InitEmployees")]
        [HttpGet]
        public HttpResponseMessage InitEmployees()
        {
            HttpResponseMessage response = null;

            try
            {
                List<EmployeeResponse> responseList = new List<EmployeeResponse>(); 
                var employeesDTO = this.employeeServ.GetEmployees();

                foreach(var employeeDTO in employeesDTO)
                {
                    responseList.Add(
                        new EmployeeResponse()
                        {
                            Id = employeeDTO.Id,
                            Name = employeeDTO.Name + " " + employeeDTO.LastName,
                            Position = employeeDTO.Position,
                            Photo = FileProvider.GetBytesFromPath(employeeDTO.PhotoPath)
                        });
                }

                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(responseList));
                
            }
            catch (Exception e)
            {
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(JsonConvert.SerializeObject(e.Message));
            }

            return response;
        }

        [ActionName("InitVisiters")]
        [HttpGet]
        public HttpResponseMessage InitVisiters()
        {
            HttpResponseMessage response = null;

            try
            {
                List<VisitorResponse> responseList = new List<VisitorResponse>();
                var visitersDTO = this.visitorServ.GetVisitors().Where(v => v.DepartureTime == null);

                foreach (var visiterDTO in visitersDTO)
                {
                    responseList.Add(
                        new VisitorResponse()
                        {
                            Id = visiterDTO.Id,
                            FirstName = visiterDTO.Name,
                            LastName = visiterDTO.LastName,
                            BadgeNumber = visiterDTO.BadgeNumber,
                            CheckInDate = visiterDTO.ArrivingTime,
                            CompanyName = visiterDTO.CompanyName
                        });
                }

                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(responseList));

            }
            catch (Exception e)
            {
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(JsonConvert.SerializeObject(e.Message));
            }

            return response;
        }

        [ActionName("CheckIfChangesMadeOnServer")]
        [HttpGet]
        public async Task<HttpResponseMessage> CheckIfChangesMadeOnServer()
        {
            HttpResponseMessage response = null;
            try
            {
                var changesMadeOnServer = await InformationUpdateProvider.GetStatus();

                if(changesMadeOnServer.IsChangesMadeToEmployees)
                {
                    InformationUpdateProvider.UnChangeStatus("employees");
                }

                if (changesMadeOnServer.IsChangesMadeToVisitors)
                {
                    InformationUpdateProvider.UnChangeStatus("visitors");
                }

                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(changesMadeOnServer));

            }
            catch (Exception e)
            {
                response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(JsonConvert.SerializeObject(e.Message));
            }

            return response;
        }

        [ActionName("RegisterVisit")]
        [HttpPost]
        public async Task<HttpResponseMessage> RegisterVisit()
        {
            HttpResponseMessage response = null;

            try
            {
                var jsonContent = await Request.Content.ReadAsStringAsync();
                var jsonConvertEntity = JsonConvert.DeserializeObject<JsonConvertEntity>(jsonContent);

                if (jsonConvertEntity.Employee != null && jsonConvertEntity.Visitor != null)
                {
                    var tempVisitor = new VisitorDTO()
                    {
                        Name = jsonConvertEntity.Visitor.FirstName,
                        LastName = jsonConvertEntity.Visitor.LastName,
                        ArrivingTime = jsonConvertEntity.Visitor.CheckInDate.ToString("MM/dd/yyyy h:mm tt"),
                        CompanyName = jsonConvertEntity.Visitor.CompanyName,
                        BadgeNumber = BadgeGenerator.GenerateNubmer(),
                    };

                    var visitorId = this.visitorServ.AddVisitorGetId(tempVisitor);

                    var newAppointment = new AppointmentDTO()
                    {
                        EmployeeId = jsonConvertEntity.Employee.Id,
                        VisitorId = visitorId,
                        Number = tempVisitor.BadgeNumber,
                        TimeOfVisit = tempVisitor.ArrivingTime
                    };

                    this.appointmentServ.AddAppointment(newAppointment);


                    var respVisiter = new VisitorResponse()
                    {
                        Id = tempVisitor.Id,
                        FirstName = tempVisitor.Name,
                        LastName = tempVisitor.LastName,
                        BadgeNumber = tempVisitor.BadgeNumber,
                        CheckInDate = tempVisitor.ArrivingTime,
                        CompanyName = tempVisitor.CompanyName
                    };

                    response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StringContent(JsonConvert.SerializeObject(respVisiter));
                    return response;
                }
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = e.Message;
                return response;
            }

            response.StatusCode = HttpStatusCode.NoContent;
            return response;
        }

        [ActionName("CheckOutVisitor")]
        [HttpPost]
        public async Task<HttpResponseMessage> CheckOutVisitor()
        {
            HttpResponseMessage response = null;

            try
            {
                var jsonContent = await Request.Content.ReadAsStringAsync();
                var visitorResponse = JsonConvert.DeserializeObject<VisitorResponse>(jsonContent);

                VisitorDTO vDTO = new VisitorDTO() {
                    Id = visitorResponse.Id,
                    ArrivingTime = visitorResponse.CheckInDate,
                    BadgeNumber = visitorResponse.BadgeNumber,
                    CompanyName = visitorResponse.CompanyName,
                    LastName = visitorResponse.LastName,
                    Name = visitorResponse.FirstName,
                    DepartureTime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt")
                };

                if (visitorResponse != null)

                    this.visitorServ.EditVisitor(vDTO);

                    response = new HttpResponseMessage(HttpStatusCode.OK);
                    response.Content = new StringContent("true");

                    return response;
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = e.Message;
                return response;
            }

            response.StatusCode = HttpStatusCode.NoContent;
            return response;
        }
    }
}
