﻿using System.Web.Mvc;

namespace VisitorRegisrationSystemServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Visitor Registration System";

            return View();
        }
    }
}
