﻿using System;
using System.Linq;

namespace VisitorRegisrationSystemServer.Helpers
{
    public static class BadgeGenerator
    {
        //Number generator
        public static string GenerateNubmer()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}