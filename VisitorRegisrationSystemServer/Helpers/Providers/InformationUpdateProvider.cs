﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using VisitorRegisrationSystemServer.Models;

namespace VisitorRegisrationSystemServer.Helpers.Providers
{
    public static class InformationUpdateProvider
    {
        public static async Task<ChangesMadeOnServer> GetStatus()
        {
            try
            {
                string targetFolder = HttpContext.Current.Server.MapPath("~/Content/statusFile/");
                string targetPath = Path.Combine(targetFolder, "isIformationUpdated.txt");

                System.IO.FileStream fs = new System.IO.FileStream(targetPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read);

                string res;
                var sr = new System.IO.StreamReader(fs);

                using (sr)
                {
                    res = await sr.ReadToEndAsync();
                }
                    
                return JsonConvert.DeserializeObject<ChangesMadeOnServer>(res);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static async Task ChangeStatus(string changesMadeIn)
        {
            try
            {
                string targetFolder = HttpContext.Current.Server.MapPath("~/Content/statusFile/");
                string targetPath = Path.Combine(targetFolder, "isIformationUpdated.txt");

                if (File.Exists(targetPath))
                {
                    var result = await GetStatus();

                    if (changesMadeIn == "employees")
                    {
                        result.IsChangesMadeToEmployees = true;
                    }

                    if(changesMadeIn == "visitors")
                    {
                        result.IsChangesMadeToVisitors = true;
                    }

                    System.IO.File.WriteAllText(targetPath, JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static async void UnChangeStatus(string changesMadeIn)
        {
            try
            {
                string targetFolder = HttpContext.Current.Server.MapPath("~/Content/statusFile/");
                string targetPath = Path.Combine(targetFolder, "isIformationUpdated.txt");

                if (File.Exists(targetPath))
                {
                    var result = await GetStatus();

                    if (changesMadeIn == "employees")
                    {
                        result.IsChangesMadeToEmployees = false;
                    }

                    if (changesMadeIn == "visitors")
                    {
                        result.IsChangesMadeToVisitors = false;
                    }

                    System.IO.File.WriteAllText(targetPath, JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private static void GetStatusFromFile()
        {
            try
            {
                string targetFolder = HttpContext.Current.Server.MapPath("~/Content/statusFile/");
                string targetPath = Path.Combine(targetFolder, "isIformationUpdated.txt");

                if (File.Exists(targetPath))
                {
                    System.IO.File.WriteAllText(targetPath, "true");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }



        //public static void UpdateStatus(bool status)
        //{
        //    try
        //    {
        //        string targetFolder = HttpContext.Current.Server.MapPath("~/Content/statusFile/");
        //        string targetPath = Path.Combine(targetFolder, "isIformationUpdated.txt");

        //        System.IO.FileStream fs = new System.IO.FileStream(targetPath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

        //        var sr = new System.IO.StreamWriter(fs);
        //        sr.Write(status.ToString());

        //        fs.Flush();
        //        fs.Close();

        //        if (!File.Exists(path))
        //        {
        //            // Create a file to write to.
        //            string createText = "Hello and Welcome" + Environment.NewLine;
        //            File.WriteAllText(path, createText);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception(e.Message);
        //    }
        //}
    }
}