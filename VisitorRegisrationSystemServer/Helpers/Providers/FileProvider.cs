﻿using System;
using System.IO;
using System.Web;

namespace VisitorRegisrationSystemServer.Helpers.Providers
{
    public static class FileProvider
    {
        public static string SaveFile(HttpPostedFileBase file)
        {
            try
            {
                char[] delimiterChars = { '.' };
                string[] words = file.FileName.Split(delimiterChars);

                var name = Guid.NewGuid().ToString() + "." + words[1];
                string targetFolder = HttpContext.Current.Server.MapPath("~/Content/contentImages/");
                string targetPath = Path.Combine(targetFolder, name);
                file.SaveAs(targetPath);

                return "~/Content/contentImages/" + name;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public static string DeleteFile(string path)
        {
            try
            {
                string targetPath = HttpContext.Current.Server.MapPath(path);

                if (System.IO.File.Exists(targetPath))
                {
                    System.IO.File.Delete(targetPath);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return null;
        }

        public static byte[] GetBytesFromPath(string path)
        {
            try
            {
                string targetPath = HttpContext.Current.Server.MapPath(path);

                if (System.IO.File.Exists(targetPath))
                {
                    return System.IO.File.ReadAllBytes(targetPath);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return null;
        }
    }
}